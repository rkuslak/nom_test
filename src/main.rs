extern crate nom;

use nom::bytes::complete::take_until;
use nom::bytes::streaming::{is_a, take_till1};
use nom::character::streaming::space0;

type ParseResult<T> = Result<T, &'static str>;

/// Macro to provide a inline test of a given character from a &str matching any of the provided
/// characters.
macro_rules! char_in {
    ( $input: expr, $( $x: tt), + ) => {
        {
            $(
                if ($input == $x) {
                    return true;
                }
            )*
            false
        }
    };
}

/// Reads the given input payload, returning the consumed values until the EOL and the input
/// consuming the EOL on success or the `nom::Err::Incomplete` error on failure
fn take_till_windows_eol(input: &str) -> nom::IResult<&str, &str> {
    let (input, value) = take_until("\r\n")(input)?;
    let (input, _) = is_a("\r\n")(input)?;
    Ok((input, value))
}

/// Assuming we have a payload of key:value pairs delimited by a Windows EOL for records, and '='
/// for key to value, finds the key from the start of a line. Returns the `nom::Err::Incomplete`
/// error on failure to find any valid key.
fn get_label(input: &str) -> nom::IResult<&str, &str> {
    let (input, _) = space0(input)?;
    let (input, label) = take_till1(|c| char_in!(c, '=', ' ', '\t'))(input)?;

    let (input, _) = space0(input)?;
    let (input, _) = is_a("=")(input)?;

    Ok((label, input))
}

/// Assuming we have a payload that has already located a key value for a given line, parses the
/// remainder of the line to locate the "value" for that line. Any trailing non-print space will be
/// returned with the value, but any leading space will be consumed and ignored. Returns the
/// `nom::Err::Incomplete` error on failure to find any character values.
fn get_value(input: &str) -> nom::IResult<&str, &str> {
    let (input, _) = space0(input)?;

    // If we don't have another delimiter, assume we've exhausted the matches in the payload
    let (input, value) = match take_till_windows_eol(input) {
        Ok((input, value)) => (input, value),
        _ => ("", input),
    };
    Ok((value, input))
}

/// Attempts to read the given line until the Windows EOL marker, consuming it and returning the
/// key and value pair with the remaining input on success, or a Error<'str> on failure describing
/// the failure reason.
fn parse_line(input: &str) -> ParseResult<(&str, &str, &str)> {
    // TODO: Convert the error reasons into a enum?
    let (label, input) = get_label(input).map_err(|_| "Failed to find label")?;
    let (value, input) = get_value(input).map_err(|_| "Failed to find value")?;
    Ok((label, value, input))
}

fn main() {
    let args: Vec<String> = std::env::args().collect();

    let mut input = match args.get(1) {
        Some(test_str) => test_str.as_str(),
        _ => "foo = bar\r\nquux=  qux baz\t a \t\r\nzoom=zan\r\n=foo\r\nya=dude",
    };

    let mut line = 1u64;
    let mut map: std::collections::HashMap<String, String> = std::collections::HashMap::new();

    while !input.is_empty() {
        match parse_line(input) {
            Ok((label, value, remaining)) => {
                map.insert(label.to_owned(), value.to_owned());
                input = remaining;
            }
            Err(e) => {
                match take_till_windows_eol(input) {
                    // Error in parsing; input will still be set to the location we started trying
                    // to parse for a value
                    Ok((remaining, error_line)) => {
                        println!("LINE {}: {}; input: {}", line, e, error_line);
                        input = remaining;
                    }
                    _ => {
                        println!("LINE {}: {}; input: {}", line, e, input);
                        input = "";
                    }
                };
            }
        }

        // If we have nothing but non-print space remaining, zero out payload indicator. If payload
        // is empty, break
        let result: nom::IResult<&str, _> = space0(input);
        if let Ok((result, _)) = result {
            input = result;
        } else {
            input = "";
        }

        line += 1;
    }

    let mut keys: Vec<&String> = map.keys().collect();
    keys.sort();
    for key in keys {
        println!("key: \'{}\' -> \'{}\'", key, map.get(key).unwrap());
    }
}
